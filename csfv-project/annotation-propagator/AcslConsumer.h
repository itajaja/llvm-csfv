//===- AcslConsumer.h - AST Consumer for ACSL -------------------*- C++ -*-===//
//
//         The LLVM Compiler Infrastructure - CSFV Annotation Framework
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements an AST consumer that takes care of the ACSL comments
//
//===----------------------------------------------------------------------===//

#include "clang/AST/ASTConsumer.h"
#include "clang/AST/DeclGroup.h"

namespace CSFV{

class AcslConsumer : public clang::ASTConsumer
{
public:
    AcslConsumer() : clang::ASTConsumer() { }
    virtual ~AcslConsumer() { }

    virtual bool HandleTopLevelDecl(clang::DeclGroupRef d){
      return clang::ASTConsumer::HandleTopLevelDecl(d);
    }
};
}//end of namespace CSFV
