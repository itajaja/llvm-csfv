//===--- main.cpp ---------------------------------------------------------===//
//
//         The LLVM Compiler Infrastructure - CSFV Annotation Framework
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include <iostream>

#include "CompilerFactory.h"
#include "AcslConsumer.h"
#include "Debug.h"

#include "clang/Basic/FileManager.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/Parse/ParseAST.h"
#include "clang/FrontendTool/Utils.h"

bool ignoreDiagnosticErrors = false;
StringRef filename;

bool parseArgs(int argc, char const *argv[]){
  int count;
  bool filechosen = false;
  bool ret = true;
   for( count = 1; count < argc; count++ ){
    //TODO: for now it checks only the first characthers, e.g.: -g = -ghad = -gw
    if (*argv[count]=='-')
    {
      char opt = *(argv[count]+1);
      switch(opt){
        case 'n':
          DEBUG("option n");
          ignoreDiagnosticErrors=true;
          break;
        case 'i':
          DEBUG("option i");
          break;
        default:
          std::cerr << "unknown option: ";
          std::cerr << argv[count];
          std::cerr << std::endl;
          ret = false;
          break;
      }
    }
    else{ //is the input file
      if(filechosen){
        std::cerr << "error: multiple input files";
        std::cerr << std::endl;
        ret = false;
      }
      DEBUG("file " << argv[count] << " chosen as input");
      filechosen = true;
      filename = argv[count];
    }
  }
  if(!filechosen){
    std::cerr << "input file not specified";
    std::cerr << std::endl;
    ret = false;
  }
  return ret;
}

int main(int argc, char const *argv[]){
  if(!parseArgs(argc, argv)){
    // parsing failed
    DEBUG("Parsing failed");
    return 0;
  }
  CompilerInstance ci;
  CSFV::CompilerFactory::GetCompilerInstance(ci);
  //CSFV::CompilerFactory::UsePredefineHeaders(ci);
  ASTConsumer *ast = new CSFV::AcslConsumer();
  ci.setASTConsumer(ast);
  ci.createASTContext();
  //ci.createSema(clang::TU_Complete, NULL);
  // open the file
  const FileEntry *pFile = ci.getFileManager().getFile(filename);
  ci.getSourceManager().createMainFileID(pFile);
  ci.getDiagnosticClient().BeginSourceFile(ci.getLangOpts(),
                                            &ci.getPreprocessor());
  // option to allow the lexer tokenize the comments
  ci.getPreprocessor().SetCommentRetentionState(true, true);
  // parse the AST
  clang::ParseAST(ci.getPreprocessor(), ast, ci.getASTContext());
  ci.getDiagnosticClient().EndSourceFile();
}
