//===- Debug.h - Compiler Instance Factory ----------------------*- C++ -*-===//
//
//         The LLVM Compiler Infrastructure - CSFV Annotation Framework
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements a handy way of adding debugging information to your
// code.
//
//===----------------------------------------------------------------------===//
namespace CSFV{

#define DBG 1
#ifdef DBG
#define DEBUG(X)  \
std::cerr << X;   \
std::cerr << std::endl;                     
#endif

}
