//===- CompilerFactory.cpp - Compiler Instance Factory ----------*- C++ -*-===//
//
//         The LLVM Compiler Infrastructure - CSFV Annotation Framework
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include "CompilerFactory.h"

#include "llvm/Support/Host.h"
#include "llvm/ADT/IntrusiveRefCntPtr.h"

#include "clang/Basic/DiagnosticOptions.h"
#include "clang/Basic/TargetOptions.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "clang/Frontend/Utils.h"
#include "clang/Lex/HeaderSearch.h"

namespace CSFV{

  void CompilerFactory::GetCompilerInstance(CompilerInstance &ci){
    DiagnosticOptions *diagOpts = new DiagnosticOptions();
    TextDiagnosticPrinter* diagPrinter =
      new TextDiagnosticPrinter(llvm::outs(), diagOpts, true);
    ci.createDiagnostics(diagPrinter);

    llvm::IntrusiveRefCntPtr<TargetOptions> pto (new TargetOptions());
    pto->Triple = llvm::sys::getDefaultTargetTriple();
    TargetInfo *pti =
      TargetInfo::CreateTargetInfo(ci.getDiagnostics(), pto.getPtr());
    ci.setTarget(pti);

    ci.createFileManager();
    ci.createSourceManager(ci.getFileManager());
    ci.createPreprocessor();
    return;
  }

  void CompilerFactory::UsePredefineHeaders(CompilerInstance &ci){
    //Initialize the preprocessor with the compiler and
    // target specific predefines.
    ci.getPreprocessorOpts().UsePredefines = true;
    llvm::IntrusiveRefCntPtr<clang::HeaderSearchOptions> hso
      (new clang::HeaderSearchOptions());
    HeaderSearch headerSearch(hso, ci.getFileManager(),
      ci.getDiagnostics(), ci.getLangOpts(), &ci.getTarget());
    // <Warning!!> -- Platform Specific Code lives here
    // This depends on A) that you're running linux and
    // B) that you have the same GCC LIBs installed that
    // I do.
    #ifdef __linux__
      hso->AddPath("/usr/lib/gcc/x86_64-linux-gnu/4.7.2/include",
        clang::frontend::Angled, false, false);
      hso->AddPath("/usr/include", clang::frontend::Angled, false, false);
    #endif
    // </Warning!!> -- End of Platform Specific Code
    
    clang::InitializePreprocessor(ci.getPreprocessor(), 
      ci.getPreprocessorOpts(), *hso, ci.getFrontendOpts());
  }

} //end of namespace CSFV
