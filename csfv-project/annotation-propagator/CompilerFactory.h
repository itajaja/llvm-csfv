//===- CompilerFactory.h - Compiler Instance Factory ------------*- C++ -*-===//
//
//         The LLVM Compiler Infrastructure - CSFV Annotation Framework
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements static methods to create and instantiate a compiler
// instance
//
//===----------------------------------------------------------------------===//

//If they are not defined we have an error at compile time
#define __STDC_LIMIT_MACROS
#define __STDC_CONSTANT_MACROS

#include "clang/Frontend/CompilerInstance.h"


using namespace clang;

namespace CSFV{

class CompilerFactory
{
public:

  CompilerFactory();

  ~CompilerFactory();

  /**
   * \brief Generate and returns a compiler instance object
   * \param ci the target instance
   */
  static void GetCompilerInstance(CompilerInstance &ci);

  /**
   * \brief Let the Compiler instance use the predefined libraries
   * \param ci the target instance
   */
  static void UsePredefineHeaders(CompilerInstance &ci);
};
} //end of namespace CSFV
