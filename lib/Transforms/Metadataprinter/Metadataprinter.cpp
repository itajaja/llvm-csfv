//===- Metadataprinter.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Metadataprinter World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "Metadataprinter"
#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instruction.h"
#include "llvm/Pass.h"
#include "llvm/IR/Value.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/IR/Metadata.h"
#include "llvm/Analysis/CallGraph.h"
#include "llvm/Support/CallSite.h"
#include "llvm/Analysis/CallGraph.h"
//#include "node.h"

//extern NBlock* programBlock;
//extern int yyparse();

using namespace llvm;

//STATISTIC(MetadataprinterCounter, "Counts number of functions greeted");

namespace {
    
    struct Metadataprinter : public ModulePass {
        static char ID;
        Metadataprinter() : ModulePass(ID) {}

        virtual bool runOnModule(Module &M) {
            // M.print(errs(), NULL);

//            for (Module::const_named_metadata_iterator I = M.named_metadata_begin(),
//                E = M.named_metadata_end(); I != E; ++I) {
//                const NamedMDNode *NMD = I;
//                errs() << "1) The module has named metadata:\n";
//                NMD->print(errs());
//                errs() << "with operands: \n";
//                for (unsigned int j=0; j<NMD->getNumOperands(); j++) {
//                    errs() << "\t ";
//                    NMD->getOperand(j)->print(errs());
//                }
//                errs() << "\n";
//                
//            }
//
//            errs() << "2) Printing instruction with an attached acsl_metadata:\n";
//            for (Module::iterator FI = M.begin(), E = M.end(); FI != E; ++FI) {
//                
//                Function *F = &*FI;
//                errs() << "\tAnalizing function: ";
//                errs().write_escaped(F->getName()) << '\n';
//                for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
//                    Instruction *pinst = &*I;
//                    
//                    if(pinst->hasMetadata()){
//                        if(pinst->getMetadata(std::string("acsl_metadata"))!=NULL){
//                            errs() <<"\t\tThe instruction: "<< *I << "\n";
//                            MDNode * metaNode = pinst->getMetadata(std::string("acsl_metadata"));
//                            errs() << "\t\t\thas metadata: ";
//                            metaNode->getOperand(0)->print(errs());
//                            errs() << "\n";
//                        }
//                    }
//                }
//                errs() << "\n";
//            }
//            
//            errs() << "3) Printing dbg metadata associated with instructions in a module:\n";
//            for (Module::iterator FI = M.begin(), E = M.end(); FI != E; ++FI) {
//                
//                Function *F = &*FI;
//                errs() << "\tAnalizing function: ";
//                errs().write_escaped(F->getName()) << '\n';
//                for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
//                    Instruction *pinst = &*I;
//                    if(pinst->hasMetadata()){
//                        if(pinst->getMetadata(std::string("dbg"))!=NULL){
//                            errs() <<"\t\tThe instruction: "<< *I << "\n";
//                            MDNode * metaNode = pinst->getMetadata(std::string("dbg"));
//                            errs() << "\t\t\thas metadata: ";
//                            metaNode->print(errs());
//                            errs() << "\n";
//                        }
//                    }
//
//                }
//                errs() << "\n";
//            }
            errs() << "Printing string declaration instruction inside functions:\n";
            for (Module::iterator FI = M.begin(), E = M.end(); FI != E; ++FI) {
                
                Function *F = &*FI;
                
                errs() << "\tAnalizing function: ";
                errs().write_escaped(F->getName()) << '\n';
                for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
//                    if (AllocaInst * AI = dyn_cast<AllocaInst>(I.getInstructionIterator())) {
//                        //Save the place of the acsl string allocation and the name of the variable
//                        
//                    } else
                    if(StoreInst * SI = dyn_cast<StoreInst>(I.getInstructionIterator()) ){
                        //recognize the right annotation
//                        SI->getOperandUse(1)->print(errs());
//                        errs()<<"\n";
                        
                        //get the string annotation
                        SI->getOperandUse(0)->print(errs());
                        if (GetElementPtrInst * GEPI = dyn_cast<GetElementPtrInst>(SI->getOperandUse(0))) {
                            GEPI->print(errs());
                            errs()<<"\n";
                        }
                        errs()<<"\n";
                        //parse the string annotation
                        
                        //store the annotation information
                    }
                    
                }
                errs() << "\n";
            }
            
//            errs() << "3) Printing metadata as a function arguments:\n";
//            for (Module::iterator FI = M.begin(), E = M.end(); FI != E; ++FI) {
//                
//                Function *F = &*FI;
//                errs() << "\tAnalizing function: ";
//                errs().write_escaped(F->getName()) << '\n';
//                for (Function::arg_iterator I = F->arg_begin(), E = F->arg_end();
//                     I != E; ++I){
//                    if (I->getType()->isMetadataTy()) {
//                      errs() << "\t\tmetadata function attribute: ";
//                    }
//                }
//                errs() << "\n";
//            }

            return false;
        }
        
        
    };
    
// Using a FunctionPass:
    
//    struct Metadataprinter : public FunctionPass {
//    static char ID; // Pass identification, replacement for typeid
//    Metadataprinter() : FunctionPass(ID) {}
//
//    virtual bool runOnFunction(Function &F) {
//      ++MetadataprinterCounter;
//      errs() << "Analizing function: ";
//      errs().write_escaped(F.getName()) << '\n';
//      errs() << "\n";
//
//        for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
//            Instruction *pinst = &*I;
//            if(pinst->hasMetadata()){
//                errs() <<"The instruction: "<< *I << "\n";
//                MDNode * metaNode = pinst->getMetadata(std::string("acsl_metadata"));
//                errs() << "Has metadata: ";
//                metaNode->getOperand(0)->print(errs());
//                errs() << "\n";
//            }
//            
//        }
//      errs() << "end of function: ";
//      errs().write_escaped(F.getName()) << "\n\n";
//      
//      return false;
//    }
//  };
}

char Metadataprinter::ID = 0;
static RegisterPass<Metadataprinter> X("Metadataprinter", "Metadata Printer Pass");