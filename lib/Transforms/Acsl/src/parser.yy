/* $Id: parser.yy 48 2009-09-05 08:07:10Z tb $ -*- mode: c++ -*- */
/** \file parser.yy Contains the example Bison parser source */

%{ /*** C/C++ Declarations ***/

#include <stdio.h>
#include <string>
#include <vector>

#include "node.h"

%}

/*** yacc/bison Declarations ***/

/* Require bison 2.3 or later */
%require "2.3"

/* add debug output code to generated parser. disable this for release
 * versions. */
%debug

/* start symbol is named "start" */
%start annotation

/* write out a header file containing the token defines */
%defines

/* use newer C++ skeleton file */
%skeleton "lalr1.cc"

/* namespace to enclose parser in */
%name-prefix="example"

/* set the parser's class identifier */
%define "parser_class_name" "Parser"

/* keep track of the current position within the input */
%locations
%initial-action
{
    // initialize the initial location object
    @$.begin.filename = @$.end.filename = &driver.streamname;
};

/* The driver is passed by reference to the parser and to the scanner. This
 * provides a simple but effective pure interface, not relying on global
 * variables. */
%parse-param { class Driver& driver }

/* verbose error messages */
%error-verbose

 /*** BEGIN EXAMPLE - Change the example grammar's tokens below ***/

%union {
    std::string * stringVal;
    ACSLNode * node;
    ACSLStatements * stmts;
    ACSLExpression * expr;
    ACSLStatement * stmt;
    std::vector<ACSLExpression*> * exprvec;
    int integerVal;
}

%token			END	     0	"end of file"
%token <stringVal> TIDENTIFIER TIDENTIFIERIR TINTEGER TDOUBLE
%token <integerVal> TASSERT TREQUIRES TENSURES
%token <integerVal> TCEQ TCNE TCLT TCLE TCGT TCGE TEQUAL
%token <integerVal> TLPAREN TRPAREN TLBRACE TRBRACE TCOMMA TDOT
%token <integerVal> TPLUS TMINUS TMUL TDIV

/* Define the type of node our nonterminal symbols represent.
   The types refer to the %union declaration above. Ex: when
   we call an ident (defined by union type ident) we are really
   calling an (NIdentifier*). It makes the compiler happy.
*/
%type <expr> expr
%type <expr> numeric ident
%type <stmts> stmts
%type <stmt> stmt

/* Operator precedence for mathematical operators */
%left TCOMMA
%left TEQUAL
%left TCOROR
%left TCANDAND
%left TOR
%left TAND
%left TCEQ TCNE
%left TCLT TCGT TCLE TCGE
%left TMINUS TPLUS
%left TMUL TDIV
%right TNOT

 /*** END EXAMPLE - Change the example grammar's tokens above ***/

%{

#include "driver.h"
#include "scanner.h"

/* this "connects" the bison parser in the driver to the flex scanner class
 * object. it defines the yylex() function call to pull the next token from the
 * current lexer object of the driver context. */
#undef yylex
#define yylex driver.lexer->lex

%}

%% /*** Grammar Rules ***/

 /*** BEGIN EXAMPLE - Change the example grammar rules below ***/

annotation : stmts END{}
        ;
        
stmts : stmt { driver.root.statements.push_back($<stmt>1); }
      | stmts stmt { driver.root.statements.push_back($<stmt>2); }
      ;

stmt : TASSERT expr { $$ = new ACSLAssertStatement(*$2, $1); }
     | TREQUIRES expr { $$ = new ACSLRequiresStatement(*$2, $1); }
     | TENSURES expr { $$ = new ACSLEnsuresStatement(*$2, $1); };
;


    
expr : ident { $$ = $1; }
     | numeric
     | expr TCEQ expr { $$ = new ACSLBinaryExpression(*$1, 1, *$3); }
     | expr TCNE expr { $$ = new ACSLBinaryExpression(*$1, 2, *$3);  }
     | expr TCLT expr { $$ = new ACSLBinaryExpression(*$1, 3, *$3); }
     | expr TCGT expr { $$ = new ACSLBinaryExpression(*$1, 4, *$3);  }
     | expr TCLE expr { $$ = new ACSLBinaryExpression(*$1, 5, *$3);  }
     | expr TCGE expr { $$ = new ACSLBinaryExpression(*$1, 6, *$3);  }
     | expr TCANDAND expr { $$ = new ACSLBinaryExpression(*$1, 7, *$3);  }
     | expr TCOROR expr { $$ = new ACSLBinaryExpression(*$1, 8, *$3);  }
     ;

ident : TIDENTIFIER { $$ = new ACSLIdentifier(*$1); delete $1; }
      | TIDENTIFIERIR { $$ = new ACSLIdentifierIR(*$1); delete $1; }
      ;

numeric : TINTEGER { $$ = new ACSLInteger(atol($1->c_str())); delete $1; }
        | TDOUBLE { $$ = new ACSLDouble(atof($1->c_str())); delete $1; }
        ;

 /*** END EXAMPLE - Change the example grammar rules above ***/

%% /*** Additional Code ***/

void example::Parser::error(const Parser::location_type& l,
			    const std::string& m)
{
    driver.error(l, m);
}
