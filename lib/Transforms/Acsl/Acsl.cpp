//===- Acsl.h - Acsl Transformation Pass ------------------------*- C++ -*-===//
//
//         The LLVM Compiler Infrastructure - CSFV Annotation Framework
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements a transformation pass that uses ACSL annotations
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "acsl"

#include "llvm/Pass.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Operator.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Debug.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/IR/Metadata.h"

#include "AcslParser.h"
#include "src/node.h"
#include "src/driver.h"

#include <iostream>
#include <sstream>


using namespace llvm;

STATISTIC(AcslCounter, "Counts number of functions greeted");

namespace {
    
  struct Acsl : public FunctionPass {
    static char ID; // Pass identification, replacement for typeid
    Acsl() : FunctionPass(ID) {}
  
      class VarMappingInfo {
      public:
          Value * context;
          StringRef nameIR;
          VarMappingInfo(Value * context, StringRef nameIR): context(context), nameIR(nameIR){}
      };
    /**
    * The runOnFunction method must be implemented by your subclass to do the
    * transformatiostoren or analysis work of your pass. As usual, a true value
    * should be returned if the function is modified.
    */
    virtual bool runOnFunction(Function &F) {
        ++AcslCounter;
        
    //ACSL ASSERTION VARIABLE MAPPING
        //FILL A DATA STRUCTURE WITH THE INFORMATIONS GATHERED BY THE DUBUG INFO
        StringMap< std::vector<VarMappingInfo> > mappingMap;        
        for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
            if(CallInst *CI = dyn_cast<CallInst>(&*I)){
                if( CI->getCalledFunction()->getName()==StringRef("llvm.dbg.declare") ){
                    MDNode * MD1 = cast<MDNode>(CI->getOperand(0));
                    StringRef nameIR = MD1->getOperand(0)->getName();
                    
                    MDNode * MD2 = cast<MDNode>(CI->getOperand(1));
                    StringRef nameSC = MD2->getOperand(2)->getName();
                    Value * context = MD2->getOperand(1);
                    
                    mappingMap[nameSC].push_back(VarMappingInfo(context, nameIR));
                }
            }
        }
        
        //ITERATE OVER THE INSTRUCTION
            //GATHER THE ACSL ANNOTATION STRING
            for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
                if(StoreInst *SI = dyn_cast<StoreInst>(&*I)){
                    
                    //TRUST JJ
                    if(ConcreteOperator<Operator,Instruction::GetElementPtr> *PI =
                       dyn_cast<ConcreteOperator<Operator,Instruction::GetElementPtr> >
                       (SI->getValueOperand())) {
                        if(GlobalVariable *CI = dyn_cast<GlobalVariable>(PI->getOperand(0))){
                            if (ConstantDataArray *GV =
                                dyn_cast<ConstantDataArray>(CI->getInitializer())){
                                if(GV->isString()){
                                    
                                    //drop front to remove the @ and the last (strange symbol from jj)
                                    StringRef annotation = GV->getAsString().drop_back(1).drop_front(1);
                                    
                                    //PARSE THE ANNOTATION
                                    ACSLStatements root;
                                    example::Driver driver(root);
                                    bool result = driver.parse_string(annotation, "input");
                                    if (result){
                                        DEBUG(errs() << driver.root.getTreePrintString() <<'\n');
                                        
                                        //GET THE VARIABLES IN THE ANNOTATION
                                        std::set<std::string> varList = driver.root.getTreeVariables();

                                        for (std::set<std::string>::iterator j = varList.begin(); j != varList.end(); ++j)
                                        {
                                            //GET THE ANNOTATION CONTEXT
                                            MDNode * MD = SI->getMetadata(std::string("dbg"));
                                            MDNode * contextMD = cast<MDNode>(MD->getOperand(2));
                                            
                                            std::string nameSC = *j;
                                            
                                            //INITIALIZATION AS A GLOBAL VARIABLE
                                            std::ostringstream o;
                                            o << "@" << nameSC;
                                            std::string nameIR = o.str();
                                            
                                            
                                            //LOOKUP THE DATA STRUCTURE FOR THE RIGHT NAME MAPPING
                                            bool hasSuperContext= true;
                                            bool foundName = false;
                                            
                                            while (hasSuperContext && !foundName) {
                                                for (std::vector<VarMappingInfo>::iterator z = mappingMap[nameSC].begin(); z!=mappingMap[nameSC].end(); ++z) {
                                                    errs() << "COMP CONT { " << contextMD << " , " << (*z).context << '\n';
                                                    //CHECK IF THERE IS A DECLARATION IN THE SAME CONTEXT
                                                    if(contextMD == (*z).context){
                                                        std::ostringstream o;
                                                        o << "%" << (*z).nameIR.str();
                                                        nameIR = o.str();
                                                        foundName=true;
                                                        break;
                                                    }
                                                }
                                                //SWITCH TO THE TOP CONTEXT FOR THE NEXT ITERATION IF NEEDED
                                                if(MDNode * MDP = dyn_cast<MDNode>(contextMD->getOperand(1))){
                                                    contextMD = MDP;
                                                }
                                                else{
                                                    hasSuperContext=false;
                                                }
                                                
                                            }

                                            
                                            //SUBSTITUTE THE VARIABLE NAMES
                                            driver.root.changeTreeVariableName(nameIR, nameSC);
                                            
                                        }
                                        DEBUG(errs() << driver.root.getTreePrintString() <<'\n');
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
        
    return false;
  }

  /**
  * The doInitialization method is designed to do simple initialization type
  * of stuff that does not depend on the functions being processed.
  * The doInitialization method call is not scheduled to overlap with any
  * other pass executions.
  */
  virtual bool doInitialization(Module &M){
    DEBUG(errs() << "\n\n###Acsl Transformation Pass###\n\n");
    return FunctionPass::doInitialization(M);
  }

  /**
  * The doFinalization method is an infrequently used method that is called
  * when the pass framework has finished calling runOnFunction for every 
  * function in the program being compiled.
  */
  virtual bool doFinalization(Module &M){
    DEBUG(errs() << "\n");
    return FunctionPass::doFinalization(M);
  }

//private:
//  void parseAnnotation(StringRef ann, inst_iterator I){
//    if(ann[0]=='@'){
//      DEBUG(errs() <<"\tACSL ASSERTION:" << ann 
//        << "\n\t before instruction" << "cippa\n");
//      AcslParser p(ann);
//    }
//  }

  };
}//end of anonymous namespace

char Acsl::ID = 0;
static RegisterPass<Acsl> X("acsl", "Acsl Assertion Parser Pass");
