//===- AcslConstProp.cpp - Acsl Constant Propagation ------------*- C++ -*-===//
//
//         The LLVM Compiler Infrastructure - CSFV Annotation Framework
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements a constant propagation that uses ACSL annotations
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "acsl"

#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/ConstantFolding.h"
#include "llvm/IR/Constant.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/ValueSymbolTable.h"
#include "llvm/Pass.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Support/CFG.h"
#include "llvm/Target/TargetLibraryInfo.h"
#include "llvm/ADT/APSInt.h"
#include <vector>
#include <set>
#include "string.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/Analysis/Dominators.h"

#include "Witness.h"
#include "Invariant.h"
#include "WitnessChecker.h"

#include "z3++.h"

using namespace llvm;

STATISTIC(AcslConstPropCounter, "Counts number of intructions propagated");

namespace {
struct AcslConstProp: public FunctionPass {
  static char ID; // Pass identification, replacement for typeid
  AcslConstProp() :
      FunctionPass(ID) {}

  virtual void getAnalysisUsage(AnalysisUsage &AU) const {
    AU.addRequired<TargetLibraryInfo>();
    AU.addRequired<DominatorTree>();
  }

  /**
   * The runOnFunction method must be implemented by your subclass to do the
   * transformation or analysis work of your pass. As usual, a true value
   * should be returned if the function is modified.
   */
  virtual bool runOnFunction(Function &F) {
    DEBUG(errs() << "##Const Prop - Function " << F.getName() << "\n");
    // Initialize the worklist to all of the instructions ready to process...
    std::set<Instruction*> WorkList;
    for(inst_iterator i = inst_begin(F), e = inst_end(F); i != e; ++i) {
        WorkList.insert(&*i);
    }
    bool Changed = false;
    DataLayout *TD = getAnalysisIfAvailable<DataLayout>();
    TargetLibraryInfo *TLI = &getAnalysis<TargetLibraryInfo>();

    //z3 variables initialization
    z3::context *Context = InvariantManager::globalContext();
    WitnessChecker wC(&F,Context);

//    DominatorTree* DT = &getAnalysis<DominatorTree>();

    while (!WorkList.empty()) {
      Instruction *I = *WorkList.begin();
      WorkList.erase(WorkList.begin());

      if (Constant *C = ConstantFoldInstruction(I, TD, TLI)){
        z3::expr InvariantExprS =
            createIntExpr(I,Context,STATE_S)==
                createIntExpr(C,Context,STATE_S);
        z3::expr InvariantExprT =
            createIntExpr(I,Context,STATE_T) ==
                createIntExpr(C,Context,STATE_T);
        z3::expr InvariantExprU =
            createIntExpr(I,Context,STATE_U) ==
                createIntExpr(C,Context,STATE_U);
        z3::expr InvariantExprV =
            createIntExpr(I,Context,STATE_V) ==
                createIntExpr(C,Context,STATE_V);
//        errs() << Z3_ast_to_string(*Context,InvariantExprV);

        // Add all of the users of this instruction to the worklist, they might
        // be constant propagatable now...
        // And add the invariant to all of them
        for (Value::use_iterator UI = I->use_begin(), UE = I->use_end();
             UI != UE; ++UI){
          WorkList.insert(cast<Instruction>(*UI));
          InvariantManager::addOrCreate(cast<Instruction>(*UI)->getParent(),
              InvariantExprS,InvariantExprT,InvariantExprU,InvariantExprV);
        }
        //propagate invariant to the next instructions
        for(inst_iterator i = inst_begin(F), e = inst_end(F); i != e; ++i) {
//          errs() << "\n"<< I->getName() << " dominates " << (*i) << "?";
//          if(DT->dominates(I,&*i)){
            InvariantManager::addOrCreate(i->getParent(),
                InvariantExprS,InvariantExprT,InvariantExprU,InvariantExprV);
//            errs() << "yes!";
//          }
        }

        //Replace all of the uses of a variable with uses of the constant.
        I->replaceAllUsesWith(C);
        WorkList.erase(I);
        Changed = true;
        ++AcslConstPropCounter;
      }
    }

    wC.buildBlockRelation(true);
    DEBUG(InvariantManager::dumpInvariants(&F));
//    errs() << "\n";
    generateWitness(F,wC);
    DEBUG(wC.dumpExpressions());
    assert(wC.checkWitness() && "witness check failed. aborting.");
    wC.propagateInvariants();
    return Changed;
  }

  /**
   * The doInitialization method is designed to do simple initialization type
   * of stuff that does not depend on the functions being processed.
   * The doInitialization method call is not scheduled to overlap with any
   * other pass executions.
   */
  virtual bool doInitialization(Module &M) {
    DEBUG(errs() << "\n###Acsl Const Prop###\n\n");
    return FunctionPass::doInitialization(M);
  }

  /**
   * The doFinalization method is an infrequently used method that is called
   * when the pass framework has finished calling runOnFunction for every
   * function in the program being compiled.
   */
  virtual bool doFinalization(Module &M) {
    DEBUG(errs() << "\n");
    return FunctionPass::doFinalization(M);
  }

private:

  /*
   * Witness Generator for ACSL Constant Propagation.
   * Two states (m,t) and (l,s) are related iff:
   * 1) π = L ^ π' = L
   * 2) forall v in V: v = v'
   * 3) forall v in Inv: a_v = Inv_v
   */
  void generateWitness(Function &F, WitnessChecker &wC){
    z3::context *C = InvariantManager::globalContext();
    z3::expr WitnessExprPrev = C->bool_val(false);
    z3::expr WitnessExprNext = C->bool_val(false);
    z3::expr piS = C->int_const(concatStrings(STATE_S,PI).data());
    z3::expr piT = C->int_const(concatStrings(STATE_T,PI).data());
    z3::expr piU = C->int_const(concatStrings(STATE_U,PI).data());
    z3::expr piV = C->int_const(concatStrings(STATE_V,PI).data());
    z3::expr WitnessClausePrev(*C);
    z3::expr WitnessClauseNext(*C);

    for (Function::iterator blk = F.begin(), e = F.end(); blk != e; ++blk){
          BasicBlock *Blk = &*blk;
          Invariant *Inv = InvariantManager::find(Blk);
          WitnessClausePrev = piS == wC.getLabel(Blk) && piU == wC.getLabel(Blk) &&
              createEqList(C,wC.getTargetVars(),STATE_S,STATE_U,PI);
          WitnessClauseNext = piT == wC.getLabel(Blk) && piV == wC.getLabel(Blk) &&
              createEqList(C,wC.getTargetVars(),STATE_T,STATE_V,PI);
          if(Inv){
            WitnessClausePrev = WitnessClausePrev && Inv->getInv(0);
            WitnessClauseNext = WitnessClauseNext && Inv->getInv(1);
          }
          WitnessExprPrev = WitnessExprPrev || WitnessClausePrev;
          WitnessExprNext = WitnessExprNext || WitnessClauseNext;
    }

    //Final State
    WitnessClausePrev = piS ==-1 && piU == -1 &&
        createEqList(C,wC.getTargetVars(),STATE_S,STATE_U,PI);
    WitnessClauseNext = piT == -1 && piV ==-1 &&
        createEqList(C,wC.getTargetVars(),STATE_T,STATE_V,PI);
    Function::iterator blk = F.end();
    blk--;
    Invariant *Inv = InvariantManager::find(&*blk);
    if(Inv){
      WitnessClausePrev = WitnessClausePrev && Inv->getInv(0);
      WitnessClauseNext = WitnessClauseNext && Inv->getInv(1);
    }
    WitnessExprPrev = WitnessExprPrev || WitnessClausePrev;
    WitnessExprNext = WitnessExprNext || WitnessClauseNext;

    wC.wus(WitnessExprPrev.simplify());
    wC.wvt(WitnessExprNext.simplify());
  }
};
} //end of anonymous namespace

char AcslConstProp::ID = 0;
static RegisterPass<AcslConstProp> X("acslcp", "acsl const propagation");
