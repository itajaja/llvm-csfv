//===- AcslDCE.cpp - Acsl Dead Code Elimination -----------------*- C++ -*-===//
//
//         The LLVM Compiler Infrastructure - CSFV Annotation Framework
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements a Dead Code Elimination that uses ACSL annotations
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "acsl"

#include "llvm/Support/Debug.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/Instruction.h"
#include "llvm/Pass.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Target/TargetLibraryInfo.h"
#include "llvm/Transforms/Utils/Local.h"
#include <vector>

#include "Witness.h"
#include "Invariant.h"
#include "WitnessChecker.h"

#include "z3++.h"

using namespace llvm;

STATISTIC(AcslDCECounter, "Counts number of instructions eliminated");

namespace {
struct AcslDCE: public FunctionPass {
  static char ID; // Pass identification, replacement for typeid
  AcslDCE() :
      FunctionPass(ID) {}

  /**
   * The runOnFunction method must be implemented by your subclass to do the
   * transformation or analysis work of your pass. As usual, a true value
   * should be returned if the function is modified.
   */
  virtual bool runOnFunction(Function &F) {
    DEBUG(errs() << "\n##DCE - Function " << F.getName() << "\n");

    InvariantManager::reset(); //TODO: temporary, it should propagate, not reset
    z3::context* C = InvariantManager::globalContext();
    WitnessChecker wC(&F,C);
    // Start out with all of the instructions in the worklist...
    std::vector<Instruction*> WorkList;
    for (inst_iterator i = inst_begin(F), e = inst_end(F); i != e; ++i)
      WorkList.push_back(&*i);

    bool Changed = false;
    //FIXME: getAnalysis doesn't work, an assertion is launched.
    TargetLibraryInfo *TLI;// = &getAnalysis<TargetLibraryInfo>();
    while (!WorkList.empty()) {
      Instruction *I = WorkList.back();
      WorkList.pop_back();
      if (isInstructionTriviallyDead(I, TLI)) {// If the instruction is dead.
        // Loop over all of the values that the instruction uses, if
        // there are instructions being used, add them to the worklist,
        // because they might go dead after this one is removed.
        for (User::op_iterator OI = I->op_begin(), E = I->op_end();
            OI != E; ++OI){
          Instruction *Used = dyn_cast<Instruction>(*OI);
          if(Used != 0)
            WorkList.push_back(Used);
        }
        // Remove the instruction.
        I->eraseFromParent();

        // Remove the instruction from the worklist if it still exists in it.
        WorkList.erase(std::remove(WorkList.begin(), WorkList.end(), I),
                       WorkList.end());
        ++AcslDCECounter;
        Changed = true;
      }
    }

//    wC.buildRelation(true);
    wC.buildBlockRelation(true);
    DEBUG(errs() << "\n");
    generateWitness(F,wC);
    DEBUG(wC.dumpExpressions());
    assert(wC.checkWitness() && "witness check failed. aborting.");
    wC.propagateInvariants();
    return Changed;
  }

  /**
   * The doInitialization method is designed to do simple initialization type
   * of stuff that does not depend on the functions being processed.
   * The doInitialization method call is not scheduled to overlap with any
   * other pass executions.
   */
  virtual bool doInitialization(Module &M) {
    DEBUG(errs() << "###Acsl DCE###\n\n");
    return FunctionPass::doInitialization(M);
  }

  /**
   * The doFinalization method is an infrequently used method that is called
   * when the pass framework has finished calling runOnFunction for every
   * function in the program being compiled.
   */
  virtual bool doFinalization(Module &M) {
    DEBUG(errs() << "\n");
    return FunctionPass::doFinalization(M);
  }

private:

  /*
   * Witness Generator for ACSL Constant Propagation.
   * Two states (m,t) and (l,s) are related iff:
   * 1) π = L ^ π' = L
   * 2) forall v in V that are live: v = v'
   */
  void generateWitness(Function &F, WitnessChecker &wC){
    z3::context *C = InvariantManager::globalContext();
    z3::expr WitnessExprPrev = C->bool_val(false);
    z3::expr WitnessExprNext = C->bool_val(false);
    z3::expr piS = C->int_const(concatStrings(STATE_S,PI).data());
    z3::expr piT = C->int_const(concatStrings(STATE_T,PI).data());
    z3::expr piU = C->int_const(concatStrings(STATE_U,PI).data());
    z3::expr piV = C->int_const(concatStrings(STATE_V,PI).data());
    z3::expr WitnessClausePrev(*C);
    z3::expr WitnessClauseNext(*C);
    for (Function::iterator blk = F.begin(), e = F.end(); blk != e; ++blk){
      BasicBlock *Blk = &*blk;
      std::vector<StringRef> sss = wC.getSourceVars();
//      Invariant *Inv = InvariantManager::find(I);
      WitnessClausePrev = piS == wC.getLabel(Blk) && piU == wC.getLabel(Blk) &&
          createEqList(C,wC.getTargetVars(),STATE_S,STATE_U,PI);
      WitnessClauseNext = piT == wC.getLabel(Blk) && piV == wC.getLabel(Blk) &&
          createEqList(C,wC.getTargetVars(),STATE_T,STATE_V,PI);
      WitnessExprPrev = WitnessExprPrev || WitnessClausePrev;
      WitnessExprNext = WitnessExprNext || WitnessClauseNext;
    }

    //Final State
    WitnessClausePrev = piS ==-1 && piU == -1 &&
        createEqList(C,wC.getTargetVars(),STATE_S,STATE_U,PI);
    WitnessClauseNext = piT == -1 && piV ==-1 &&
        createEqList(C,wC.getTargetVars(),STATE_T,STATE_V,PI);
    Function::iterator blk = F.end();
    blk--;
    Invariant *Inv = InvariantManager::find(&*blk);
    if(Inv){
      WitnessClausePrev = WitnessClausePrev && Inv->getInv(0);
      WitnessClauseNext = WitnessClauseNext && Inv->getInv(1);
    }
    WitnessExprPrev = WitnessExprPrev || WitnessClausePrev;
    WitnessExprNext = WitnessExprNext || WitnessClauseNext;

    wC.wus(WitnessExprPrev.simplify());
    wC.wvt(WitnessExprNext.simplify());
  }

};
} //end of anonymous namespace

char AcslDCE::ID = 0;
static RegisterPass<AcslDCE> X("acsldce", "acsl dead code elimination");
