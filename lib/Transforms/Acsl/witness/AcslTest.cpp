//===- AcslTest.cpp - Acsl Test Pass ----------------------------*- C++ -*-===//
//
//         The LLVM Compiler Infrastructure - CSFV Annotation Framework
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Pass used to perform simple tests for other passes
// Test should be put in the doInitialization method
//
// how to run the Test:
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "acsl"

#include "llvm/Pass.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"

#include "Witness.h"

using namespace llvm;

namespace {
struct AcslTest: public FunctionPass {
  static char ID; // Pass identification, replacement for typeid
  AcslTest() :
      FunctionPass(ID) {}

  virtual bool runOnFunction(Function &F) {
    return false;
  }

  virtual bool doInitialization(Module &M) {
    errs() << "\n###Acsl Test module###\n";
    errs() << "Performing tests...\n";
    State s;
    Type *ty = IntegerType::get(M.getContext(),8);
    Constant *cnst = ConstantInt::getAllOnesValue(ty);
    s.addValue("ciao",cnst);
    assert(s.getValue("ciao")==cnst);
    assert(s.getValue("ciao")==NULL);
    errs() << "\nAll Tests passed";
    return FunctionPass::doInitialization(M);
  }

  virtual bool doFinalization(Module &M) {
    DEBUG(errs() << "\n");
    return FunctionPass::doFinalization(M);
  }

private:

};
} //end of anonymous namespace

char AcslTest::ID = 0;
static RegisterPass<AcslTest> X("acsltest", "acsl test module");
