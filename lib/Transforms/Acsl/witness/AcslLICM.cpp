//===- AcslLICM.cpp - Acsl Loop Invariant Code Motion -----------*- C++ -*-===//
//
//         The LLVM Compiler Infrastructure - CSFV Annotation Framework
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements Loop invariant code motion that uses ACSL annotations.
// This pass performs loop invariant code motion, attempting to remove as much
// code from the body of a loop as possible.  It does this by either hoisting
// code into the preheader block, or by sinking code to the exit blocks if it is
// safe.  This pass also promotes must-aliased memory locations in the loop to
// live in registers, thus hoisting and sinking "invariant" loads and stores.
//
// This pass uses alias analysis for two purposes:
//
//  1. Moving loop invariant loads and calls out of loops.  If we can determine
//     that a load or call inside of a loop never aliases anything stored to,
//     we can hoist it or sink it like any other instruction.
//  2. Scalar Promotion of Memory - If there is a store instruction inside of
//     the loop, we try to move the store to happen AFTER the loop instead of
//     inside of the loop.  This can only happen if a few conditions are true:
//       A. The pointer stored through is loop invariant
//       B. There are no stores or loads in the loop which _may_ alias the
//          pointer.  There are no calls in the loop which mod/ref the pointer.
//     If these conditions are true, we can promote the loads and stores in the
//     loop of the pointer to use a temporary alloca'd variable.  We then use
//     the SSAUpdater to construct the appropriate SSA form for the value.
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "acsl"

#include "llvm/Transforms/Scalar.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/AliasSetTracker.h"
#include "llvm/Analysis/ConstantFolding.h"
#include "llvm/Analysis/Dominators.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/ValueTracking.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Metadata.h"
#include "llvm/Support/CFG.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Target/TargetLibraryInfo.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Transforms/Utils/SSAUpdater.h"

#include "Witness.h"
#include "Invariant.h"
#include "WitnessChecker.h"

#include "z3++.h"
#include <vector>
#include <set>
#include <algorithm>

using namespace llvm;

STATISTIC(AcslNumSunk      , "Number of instructions sunk out of loop");
STATISTIC(AcslNumHoisted   , "Number of instructions hoisted out of loop");
STATISTIC(AcslNumMovedLoads, "Number of load insts hoisted or sunk");
STATISTIC(AcslNumMovedCalls, "Number of call insts hoisted or sunk");

namespace {

struct AcslLICM: public LoopPass {
  static char ID; // Pass identification, replacement for typeid
  AcslLICM() :
      LoopPass(ID) {}

  /// This transformation requires natural loop information & requires that
  /// loop preheaders be inserted into the CFG...
  ///
  virtual void getAnalysisUsage(AnalysisUsage &AU) const {
    AU.setPreservesCFG();
    AU.addRequired<DominatorTree>();
    AU.addRequired<LoopInfo>();
    AU.addRequiredID(LoopSimplifyID);
    AU.addRequired<AliasAnalysis>();
    AU.addPreserved<AliasAnalysis>();
    AU.addPreserved("scalar-evolution");
    AU.addPreservedID(LoopSimplifyID);
    AU.addRequired<TargetLibraryInfo>();
  }

  /// Hoist expressions out of the specified loop. Note, alias info for inner
  /// loop is not preserved so it is not a good idea to run LICM multiple
  /// times on one loop.
  ///
  bool runOnLoop(Loop *L, LPPassManager &LPM) {
    errs() << "aho!\n";
    Changed = false;

    SimulationMap.clear();
    InvariantManager::reset(); //TODO: temporary, it should propagate, not reset
    z3::context* C = InvariantManager::globalContext();
    WitnessChecker wC(L->getHeader()->getParent(),C);

    // Get our Loop and Alias Analysis information...
    LI = &getAnalysis<LoopInfo>();
    AA = &getAnalysis<AliasAnalysis>();
    DT = &getAnalysis<DominatorTree>();

    TD = getAnalysisIfAvailable<DataLayout>();
    TLI = &getAnalysis<TargetLibraryInfo>();

    CurAST = new AliasSetTracker(*AA);

    CurLoop = L;

    // Get the preheader block to move instructions into...
    Preheader = L->getLoopPreheader();

    if (Preheader)
      HoistRegion(DT->getNode(L->getHeader()));

    // Clear out loops state information for the next iteration
    CurLoop = 0;
    Preheader = 0;

    // If this loop is nested inside of another one, save the alias information
    // for when we process the outer loop.
//    if (L->getParentLoop())
//      LoopToAliasSetMap[L] = CurAST;
//    else
//      delete CurAST;
    wC.buildBlockRelation(true);
    DEBUG(errs() << "\n");
    generateWitness(*L,wC);
    DEBUG(wC.dumpExpressions());
    wC.checkStutteringWitness();
//    assert(wC.checkStutteringWitness() && "witness check failed. aborting.");
    wC.propagateInvariants();

    return Changed;
  }

  bool doFinalization() {
    DEBUG(errs() << "\n");
    return LoopPass::doFinalization();
  }

private:

  /*
   * Witness Generator for ACSL Loop invariant code motion
   */
  void generateWitness(Loop &L, WitnessChecker &wC){
    Function* F = L.getExitBlock()->getParent();
    z3::context *C = InvariantManager::globalContext();

    z3::expr WitnessExprUs = C->bool_val(false);
    z3::expr WitnessExprUt = C->bool_val(false);
    z3::expr WitnessExprVs = C->bool_val(false);
    z3::expr WitnessExprVt = C->bool_val(false);

    z3::expr piS = C->int_const(concatStrings(STATE_S,PI).data());
    z3::expr piT = C->int_const(concatStrings(STATE_T,PI).data());
    z3::expr piU = C->int_const(concatStrings(STATE_U,PI).data());
    z3::expr piV = C->int_const(concatStrings(STATE_V,PI).data());

    z3::expr WitnessClauseUs(*C);
    z3::expr WitnessClauseUt(*C);
    z3::expr WitnessClauseVs(*C);
    z3::expr WitnessClauseVt(*C);
    for (Function::iterator blk = F->begin(), e = F->end(); blk != e; ++blk){
      BasicBlock *Blk = &*blk;
      Invariant *Inv = InvariantManager::find(Blk);
//      std::vector<StringRef> sss = wC.getSourceVars();
//      Invariant *Inv = InvariantManager::find(I);
      WitnessClauseUs = piU == wC.getLabel(Blk);
      WitnessClauseUt = piU == wC.getLabel(Blk);
      WitnessClauseVs = piV == wC.getLabel(Blk);
      WitnessClauseVt = piV == wC.getLabel(Blk);
      if(SimulationMap.count(Blk)){
        for (std::vector<BasicBlock*>::iterator i = SimulationMap[Blk].begin();
            i != SimulationMap[Blk].end(); ++i){
          BasicBlock* bb = *i;
          WitnessClauseUs = WitnessClauseUs || piS == wC.getLabel(bb);
          WitnessClauseUt = WitnessClauseUt || piT == wC.getLabel(bb);
          WitnessClauseVs = WitnessClauseVs || piS == wC.getLabel(bb);
          WitnessClauseVt = WitnessClauseVt || piT == wC.getLabel(bb);
        }
      }
      if(Inv){
        errs() << "inv\n";
        WitnessClauseUs = WitnessClauseUs && Inv->getInv(0);
        WitnessClauseUt = WitnessClauseUt && Inv->getInv(1);
        WitnessClauseVs = WitnessClauseVs && Inv->getInv(0);
        WitnessClauseVt = WitnessClauseVt && Inv->getInv(1);
      }
      WitnessClauseUs = piS == wC.getLabel(Blk) && (WitnessClauseUs) &&
          createEqList(C,wC.getTargetVars(),STATE_S,STATE_U,PI);
      WitnessClauseUt = piT == wC.getLabel(Blk) && (WitnessClauseUt) &&
          createEqList(C,wC.getTargetVars(),STATE_T,STATE_U,PI);
      WitnessClauseVs = piS == wC.getLabel(Blk) && (WitnessClauseVs) &&
          createEqList(C,wC.getTargetVars(),STATE_S,STATE_V,PI);
      WitnessClauseVt = piT == wC.getLabel(Blk) && (WitnessClauseVt) &&
          createEqList(C,wC.getTargetVars(),STATE_T,STATE_V,PI);
      WitnessExprUs = WitnessExprUs || WitnessClauseUs;
      WitnessExprUt = WitnessExprUt || WitnessClauseUt;
      WitnessExprVs = WitnessExprVs || WitnessClauseVs;
      WitnessExprVt = WitnessExprVt || WitnessClauseVt;
    }

    //Final State
    WitnessClauseUs = piS == -1 && piU == -1 &&
        createEqList(C,wC.getTargetVars(),STATE_S,STATE_U,PI);
    WitnessClauseUt = piT == -1 && piU == -1 &&
        createEqList(C,wC.getTargetVars(),STATE_T,STATE_U,PI);
    WitnessClauseVs = piS == -1 && piV == -1 &&
        createEqList(C,wC.getTargetVars(),STATE_S,STATE_V,PI);
    WitnessClauseVt = piT == -1 && piV == -1 &&
        createEqList(C,wC.getTargetVars(),STATE_T,STATE_V,PI);
    Function::iterator blk = F->end();
    blk--;
    Invariant *Inv = InvariantManager::find(&*blk);
    if(Inv){

    }
    WitnessExprUs = WitnessExprUs || WitnessClauseUs;
    WitnessExprUt = WitnessExprUt || WitnessClauseUt;
    WitnessExprVs = WitnessExprVs || WitnessClauseVs;
    WitnessExprVt = WitnessExprVt || WitnessClauseVt;

    wC.wus(WitnessExprUs.simplify());
    wC.wut(WitnessExprUt.simplify());
    wC.wvs(WitnessExprVs.simplify());
    wC.wvt(WitnessExprVt.simplify());
  }

  AliasAnalysis *AA;       // Current AliasAnalysis information
  LoopInfo      *LI;       // Current LoopInfo
  DominatorTree *DT;       // Dominator Tree for the current Loop.

  DataLayout *TD;          // DataLayout for constant folding.
  TargetLibraryInfo *TLI;  // TargetLibraryInfo for constant folding.

  // State that is updated as we process loops.
  bool Changed;            // Set to true when we change anything.
  BasicBlock *Preheader;   // The preheader block of the current loop...
  Loop *CurLoop;           // The current loop we are working on...
  AliasSetTracker *CurAST; // AliasSet information for the current loop...
  bool MayThrow;           // The current loop contains an instruction which
                           // may throw, thus preventing code motion of
                           // instructions with side effects.
  DenseMap<Loop*, AliasSetTracker*> LoopToAliasSetMap;

  /// cloneBasicBlockAnalysis - Simple Analysis hook. Clone alias set info.
  void cloneBasicBlockAnalysis(BasicBlock *From, BasicBlock *To, Loop *L) {
    AliasSetTracker *AST = LoopToAliasSetMap.lookup(L);
    if (!AST)
      return;

    AST->copyValue(From, To);
  }

  /// deleteAnalysisValue - Simple Analysis hook. Delete value V from alias
  /// set.
  void deleteAnalysisValue(Value *V, Loop *L) {
    AliasSetTracker *AST = LoopToAliasSetMap.lookup(L);
    if (!AST)
      return;

    AST->deleteValue(V);
  }

  /// SinkRegion - Walk the specified region of the CFG (defined by all blocks
  /// dominated by the specified block, and that are in the current loop) in
  /// reverse depth first order w.r.t the DominatorTree.  This allows us to visit
  /// uses before definitions, allowing us to sink a loop body in one pass without
  /// iteration.
  ///
  void SinkRegion(DomTreeNode *N) {
    assert(N != 0 && "Null dominator tree node?");
    BasicBlock *BB = N->getBlock();

    // If this subregion is not in the top level loop at all, exit.
    if (!CurLoop->contains(BB)) return;

    // We are processing blocks in reverse dfo, so process children first.
    const std::vector<DomTreeNode*> &Children = N->getChildren();
    for (unsigned i = 0, e = Children.size(); i != e; ++i)
      SinkRegion(Children[i]);

    // Only need to process the contents of this block if it is not part of a
    // subloop (which would already have been processed).
    if (inSubLoop(BB)) return;

    for (BasicBlock::iterator II = BB->end(); II != BB->begin(); ) {
      Instruction &I = *--II;

      // If the instruction is dead, we would try to sink it because it isn't used
      // in the loop, instead, just delete it.
      if (isInstructionTriviallyDead(&I, TLI)) {
        DEBUG(dbgs() << "LICM deleting dead inst: " << I << '\n');
        ++II;
        CurAST->deleteValue(&I);
        I.eraseFromParent();
        Changed = true;
        continue;
      }

      // Check to see if we can sink this instruction to the exit blocks
      // of the loop.  We can do this if the all users of the instruction are
      // outside of the loop.  In this case, it doesn't even matter if the
      // operands of the instruction are loop invariant.
      //
      if (isNotUsedInLoop(I) && canSinkOrHoistInst(I)) {
        ++II;
        sink(I);
      }
    }
  }

  /// HoistRegion - Walk the specified region of the CFG (defined by all blocks
  /// dominated by the specified block, and that are in the current loop) in depth
  /// first order w.r.t the DominatorTree.  This allows us to visit definitions
  /// before uses, allowing us to hoist a loop body in one pass without iteration.
  ///
  void HoistRegion(DomTreeNode *N) {
    assert(N != 0 && "Null dominator tree node?");
    BasicBlock *BB = N->getBlock();

    // If this subregion is not in the top level loop at all, exit.
    if (!CurLoop->contains(BB)) return;

    // Only need to process the contents of this block if it is not part of a
    // subloop (which would already have been processed).
    if (!inSubLoop(BB))
      for (BasicBlock::iterator II = BB->begin(), E = BB->end(); II != E; ) {
        Instruction &I = *II++;

        // Try constant folding this instruction.  If all the operands are
        // constants, it is technically hoistable, but it would be better to just
        // fold it.
//        if (Constant *C = ConstantFoldInstruction(&I, TD, TLI)) {
//          DEBUG(dbgs() << "LICM folding inst: " << I << "  --> " << *C << '\n');
//          CurAST->copyValue(&I, C);
//          CurAST->deleteValue(&I);
//          I.replaceAllUsesWith(C);
//          I.eraseFromParent();
//          continue;
//        }

        // Try hoisting the instruction out to the preheader.  We can only do this
        // if all of the operands of the instruction are loop invariant and if it
        // is safe to hoist the instruction.
        //
        if (CurLoop->hasLoopInvariantOperands(&I) && canSinkOrHoistInst(I) &&
            isSafeToExecuteUnconditionally(I))
          hoist(I);
      }

    const std::vector<DomTreeNode*> &Children = N->getChildren();
    for (unsigned i = 0, e = Children.size(); i != e; ++i)
      HoistRegion(Children[i]);
  }

  /// inSubLoop - Little predicate that returns true if the specified basic
  /// block is in a subloop of the current one, not the current one itself.
  ///
  bool inSubLoop(BasicBlock *BB) {
    assert(CurLoop->contains(BB) && "Only valid if BB is IN the loop");
    return LI->getLoopFor(BB) != CurLoop;
  }

  /// sink - When an instruction is found to only be used outside of the loop,
  /// this function moves it to the exit blocks and patches up SSA form as needed.
  /// This method is guaranteed to remove the original instruction from its
  /// position, and may either delete it or move it to outside of the loop.
  ///
  void sink(Instruction &I) {
    DEBUG(dbgs() << "LICM sinking instruction: " << I << "\n");

    SmallVector<BasicBlock*, 8> ExitBlocks;
    CurLoop->getUniqueExitBlocks(ExitBlocks);

    if (isa<LoadInst>(I)) ++AcslNumMovedLoads;
    else if (isa<CallInst>(I)) ++AcslNumMovedCalls;
    ++AcslNumSunk;
    Changed = true;

    // The case where there is only a single exit node of this loop is common
    // enough that we handle it as a special (more efficient) case.  It is more
    // efficient to handle because there are no PHI nodes that need to be placed.
    if (ExitBlocks.size() == 1) {
      if (!DT->dominates(I.getParent(), ExitBlocks[0])) {
        // Instruction is not used, just delete it.
        CurAST->deleteValue(&I);
        // If I has users in unreachable blocks, eliminate.
        // If I is not void type then replaceAllUsesWith undef.
        // This allows ValueHandlers and custom metadata to adjust itself.
        if (!I.use_empty())
          I.replaceAllUsesWith(UndefValue::get(I.getType()));
        I.eraseFromParent();
      } else {
        // Move the instruction to the start of the exit block, after any PHI
        // nodes in it.
        I.moveBefore(ExitBlocks[0]->getFirstInsertionPt());

        // This instruction is no longer in the AST for the current loop, because
        // we just sunk it out of the loop.  If we just sunk it into an outer
        // loop, we will rediscover the operation when we process it.
        CurAST->deleteValue(&I);
      }
      return;
    }

    if (ExitBlocks.empty()) {
      // The instruction is actually dead if there ARE NO exit blocks.
      CurAST->deleteValue(&I);
      // If I has users in unreachable blocks, eliminate.
      // If I is not void type then replaceAllUsesWith undef.
      // This allows ValueHandlers and custom metadata to adjust itself.
      if (!I.use_empty())
        I.replaceAllUsesWith(UndefValue::get(I.getType()));
      I.eraseFromParent();
      return;
    }

    // Otherwise, if we have multiple exits, use the SSAUpdater to do all of the
    // hard work of inserting PHI nodes as necessary.
    SmallVector<PHINode*, 8> NewPHIs;
    SSAUpdater SSA(&NewPHIs);

    if (!I.use_empty())
      SSA.Initialize(I.getType(), I.getName());

    // Insert a copy of the instruction in each exit block of the loop that is
    // dominated by the instruction.  Each exit block is known to only be in the
    // ExitBlocks list once.
    BasicBlock *InstOrigBB = I.getParent();
    unsigned NumInserted = 0;

    for (unsigned i = 0, e = ExitBlocks.size(); i != e; ++i) {
      BasicBlock *ExitBlock = ExitBlocks[i];

      if (!DT->dominates(InstOrigBB, ExitBlock))
        continue;

      // Insert the code after the last PHI node.
      BasicBlock::iterator InsertPt = ExitBlock->getFirstInsertionPt();

      // If this is the first exit block processed, just move the original
      // instruction, otherwise clone the original instruction and insert
      // the copy.
      Instruction *New;
      if (NumInserted++ == 0) {
        I.moveBefore(InsertPt);
        New = &I;
      } else {
        New = I.clone();
        if (!I.getName().empty())
          New->setName(I.getName()+".le");
        ExitBlock->getInstList().insert(InsertPt, New);
      }

      // Now that we have inserted the instruction, inform SSAUpdater.
      if (!I.use_empty())
        SSA.AddAvailableValue(ExitBlock, New);
    }

    // If the instruction doesn't dominate any exit blocks, it must be dead.
    if (NumInserted == 0) {
      CurAST->deleteValue(&I);
      if (!I.use_empty())
        I.replaceAllUsesWith(UndefValue::get(I.getType()));
      I.eraseFromParent();
      return;
    }

    // Next, rewrite uses of the instruction, inserting PHI nodes as needed.
    for (Value::use_iterator UI = I.use_begin(), UE = I.use_end(); UI != UE; ) {
      // Grab the use before incrementing the iterator.
      Use &U = UI.getUse();
      // Increment the iterator before removing the use from the list.
      ++UI;
      SSA.RewriteUseAfterInsertions(U);
    }

    // Update CurAST for NewPHIs if I had pointer type.
    if (I.getType()->isPointerTy())
      for (unsigned i = 0, e = NewPHIs.size(); i != e; ++i)
        CurAST->copyValue(&I, NewPHIs[i]);

    // Finally, remove the instruction from CurAST.  It is no longer in the loop.
    CurAST->deleteValue(&I);
  }

  /// hoist - When an instruction is found to only use loop invariant operands
  /// that is safe to hoist, this instruction is called to do the dirty work.
  ///
  void hoist(Instruction &I) {
    DEBUG(dbgs() << "LICM hoisting to " << Preheader->getName() << ": "
          << I << "\n");

    makeMap(Preheader,CurLoop->getHeader());
    makeMap(Preheader,I.getParent());
    makeMap(CurLoop->getHeader(),I.getParent());
    // Move the new node to the Preheader, before its terminator.
    DataLayout *TD = getAnalysisIfAvailable<DataLayout>();
    TargetLibraryInfo *TLI = &getAnalysis<TargetLibraryInfo>();
    if (Constant *C = ConstantFoldInstruction(&I, TD, TLI)){
      z3::context* Context = InvariantManager::globalContext();
      z3::expr InvariantExprS =
          createIntExpr(&I,Context,STATE_S)==
              createIntExpr(C,Context,STATE_S);
      z3::expr InvariantExprT =
          createIntExpr(&I,Context,STATE_T) ==
              createIntExpr(C,Context,STATE_T);
      z3::expr InvariantExprU =
          createIntExpr(&I,Context,STATE_U) ==
              createIntExpr(C,Context,STATE_U);
      z3::expr InvariantExprV =
          createIntExpr(&I,Context,STATE_V) ==
              createIntExpr(C,Context,STATE_V);
      for (Function::iterator blk = I.getParent()->getParent()->begin(),
          e = I.getParent()->getParent()->end(); blk != e; ++blk){
        BasicBlock *Blk = &*blk;
        InvariantManager::addOrCreate(Blk,
            InvariantExprS,InvariantExprT,InvariantExprU,InvariantExprV);
      }
    }


    I.moveBefore(Preheader->getTerminator());

    if (isa<LoadInst>(I)) ++AcslNumMovedLoads;
    else if (isa<CallInst>(I)) ++AcslNumMovedCalls;
    ++AcslNumHoisted;
    Changed = true;
  }

  /// isSafeToExecuteUnconditionally - Only sink or hoist an instruction if it is
  /// not a trapping instruction or if it is a trapping instruction and is
  /// guaranteed to execute.
  ///
  bool isSafeToExecuteUnconditionally(Instruction &Inst) {
    // If it is not a trapping instruction, it is always safe to hoist.
    if (isSafeToSpeculativelyExecute(&Inst))
      return true;

    return isGuaranteedToExecute(Inst);
  }

  bool isGuaranteedToExecute(Instruction &Inst) {

    // Somewhere in this loop there is an instruction which may throw and make us
    // exit the loop.
    if (MayThrow)
      return false;

    // Otherwise we have to check to make sure that the instruction dominates all
    // of the exit blocks.  If it doesn't, then there is a path out of the loop
    // which does not execute this instruction, so we can't hoist it.

    // If the instruction is in the header block for the loop (which is very
    // common), it is always guaranteed to dominate the exit blocks.  Since this
    // is a common case, and can save some work, check it now.
    if (Inst.getParent() == CurLoop->getHeader())
      return true;

    // Get the exit blocks for the current loop.
    SmallVector<BasicBlock*, 8> ExitBlocks;
    CurLoop->getExitBlocks(ExitBlocks);

    // Verify that the block dominates each of the exit blocks of the loop.
    for (unsigned i = 0, e = ExitBlocks.size(); i != e; ++i)
      if (!DT->dominates(Inst.getParent(), ExitBlocks[i]))
        return false;

    // As a degenerate case, if the loop is statically infinite then we haven't
    // proven anything since there are no exit blocks.
    if (ExitBlocks.empty())
      return false;

    return true;
  }

  /// pointerInvalidatedByLoop - Return true if the body of this loop may
  /// store into the memory location pointed to by V.
  ///
  bool pointerInvalidatedByLoop(Value *V, uint64_t Size,
                                const MDNode *TBAAInfo) {
    // Check to see if any of the basic blocks in CurLoop invalidate *V.
    return CurAST->getAliasSetForPointer(V, Size, TBAAInfo).isMod();
  }

  /// canSinkOrHoistInst - Return true if the hoister and sinker can handle this
  /// instruction.
  ///
  bool canSinkOrHoistInst(Instruction &I) {
    // Loads have extra constraints we have to verify before we can hoist them.
    if (LoadInst *LI = dyn_cast<LoadInst>(&I)) {
      if (!LI->isUnordered())
        return false;        // Don't hoist volatile/atomic loads!

      // Loads from constant memory are always safe to move, even if they end up
      // in the same alias set as something that ends up being modified.
      if (AA->pointsToConstantMemory(LI->getOperand(0)))
        return true;
      if (LI->getMetadata("invariant.load"))
        return true;

      // Don't hoist loads which have may-aliased stores in loop.
      uint64_t Size = 0;
      if (LI->getType()->isSized())
        Size = AA->getTypeStoreSize(LI->getType());
      return !pointerInvalidatedByLoop(LI->getOperand(0), Size,
                                       LI->getMetadata(LLVMContext::MD_tbaa));
    } else if (CallInst *CI = dyn_cast<CallInst>(&I)) {
      // Don't sink or hoist dbg info; it's legal, but not useful.
      if (isa<DbgInfoIntrinsic>(I))
        return false;

      // Handle simple cases by querying alias analysis.
      AliasAnalysis::ModRefBehavior Behavior = AA->getModRefBehavior(CI);
      if (Behavior == AliasAnalysis::DoesNotAccessMemory)
        return true;
      if (AliasAnalysis::onlyReadsMemory(Behavior)) {
        // If this call only reads from memory and there are no writes to memory
        // in the loop, we can hoist or sink the call as appropriate.
        bool FoundMod = false;
        for (AliasSetTracker::iterator I = CurAST->begin(), E = CurAST->end();
             I != E; ++I) {
          AliasSet &AS = *I;
          if (!AS.isForwardingAliasSet() && AS.isMod()) {
            FoundMod = true;
            break;
          }
        }
        if (!FoundMod) return true;
      }

      // FIXME: This should use mod/ref information to see if we can hoist or
      // sink the call.

      return false;
    }

    // Only these instructions are hoistable/sinkable.
    if (!isa<BinaryOperator>(I) && !isa<CastInst>(I) && !isa<SelectInst>(I) &&
        !isa<GetElementPtrInst>(I) && !isa<CmpInst>(I) &&
        !isa<InsertElementInst>(I) && !isa<ExtractElementInst>(I) &&
        !isa<ShuffleVectorInst>(I) && !isa<ExtractValueInst>(I) &&
        !isa<InsertValueInst>(I))
      return false;

    return isSafeToExecuteUnconditionally(I);
  }
  /// isNotUsedInLoop - Return true if the only users of this instruction are
  /// outside of the loop.  If this is true, we can sink the instruction to the
  /// exit blocks of the loop.
  ///
  bool isNotUsedInLoop(Instruction &I) {
    for (Value::use_iterator UI = I.use_begin(), E = I.use_end(); UI != E; ++UI) {
      Instruction *User = cast<Instruction>(*UI);
      if (PHINode *PN = dyn_cast<PHINode>(User)) {
        // PHI node uses occur in predecessor blocks!
        for (unsigned i = 0, e = PN->getNumIncomingValues(); i != e; ++i)
          if (PN->getIncomingValue(i) == &I)
            if (CurLoop->contains(PN->getIncomingBlock(i)))
              return false;
      } else if (CurLoop->contains(User)) {
        return false;
      }
    }
    return true;
  }

private:

  DenseMap<BasicBlock*, std::vector<BasicBlock*> > SimulationMap;

  void makeMap(BasicBlock* from, BasicBlock* to){
    if(!SimulationMap.count(from))
      SimulationMap[from] = std::vector<BasicBlock*>();
    SimulationMap[from].push_back(to);
  }

};
} //end of anonymous namespace

char AcslLICM::ID = 0;
static RegisterPass<AcslLICM> X("acsllicm", "acsl LICM");
