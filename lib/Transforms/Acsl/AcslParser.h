//===- AcslParser.h - Acsl Parser -------------------------------*- C++ -*-===//
//
//         The LLVM Compiler Infrastructure - CSFV Annotation Framework
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements a parser for ACSL language
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "acsl"

using namespace llvm;

namespace {

/**
* /brief This class implements a parser for ACSL language
*/
class AcslParser{

public:
  AcslParser(StringRef annotation):
  annotation(annotation)
  {}

private:
  //contains the full annotation
  const StringRef annotation;
};

}//end of anonymous namespace
